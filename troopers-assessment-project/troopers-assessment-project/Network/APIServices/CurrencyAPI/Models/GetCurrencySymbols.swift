//
//  GetCurrencySymbols.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 30/05/2022.
//

struct GetCurrencySymbolsResponse: Codable, Hashable {
    let success: Bool
    let symbols: [String: String]
    
    init(success: Bool, symbols: [String: String]) {
        self.success = success
        self.symbols = symbols
    }
}
