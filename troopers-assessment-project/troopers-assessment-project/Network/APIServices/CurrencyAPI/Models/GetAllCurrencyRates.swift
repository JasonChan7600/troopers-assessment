//
//  GetAllCurrencyRates.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 30/05/2022.
//

struct GetAllCurrencyRatesRequest: Codable, Hashable {
    let baseCurrencyCode: String
    
    init(baseCurrencyCode: String) {
        self.baseCurrencyCode = baseCurrencyCode
    }
}

struct GetAllCurrencyRatesResponse: Codable, Hashable {
    let base: String
    let date: String
    let rates: [String: Double]
    let success: Bool
    let timestamp: Int
    
    init(base: String, date: String, rates: [String: Double], success: Bool, timestamp: Int) {
        self.base = base
        self.date = date
        self.rates = rates
        self.success = success
        self.timestamp = timestamp
    }
}
