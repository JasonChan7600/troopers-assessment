//
//  GetConvertedAmount.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 30/05/2022.
//

struct GetConvertedAmountRequest: Codable, Hashable {
    let fromCurrency: String
    let toCurrency: String
    let amount: String
    
    init(fromCurrency: String, toCurrency: String, amount: String) {
        self.fromCurrency = fromCurrency
        self.toCurrency = toCurrency
        self.amount = amount
    }
}

struct GetConvertedAmountResponse: Codable, Hashable {
    let date: String
    let info: InfoObject
    let query: QueryObject
    let result: Double
    let success: Bool
    
    init(date: String, info: InfoObject, query: QueryObject, result: Double, success: Bool) {
        self.date = date
        self.info = info
        self.query = query
        self.result = result
        self.success = success
    }
}

struct InfoObject: Codable, Hashable {
    let rate: Double
    let timestamp: Int
    
    init(rate: Double, timestamp: Int) {
        self.rate = rate
        self.timestamp = timestamp
    }
}

struct QueryObject: Codable, Hashable {
    let amount: Double
    let from: String
    let to: String
    
    init(amount: Double, from: String, to: String) {
        self.amount = amount
        self.from = from
        self.to = to
    }
}
