//
//  GetHistoryRates.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 30/05/2022.
//

struct GetHistoryRatesRequest: Codable, Hashable {
    let endDate: String
    let startDate: String
    let baseCurrency: String
    let selectedCurrencies: String
    
    init(endDate: String, startDate: String, baseCurrency: String, selectedCurrencies: String) {
        self.endDate = endDate
        self.startDate = startDate
        self.baseCurrency = baseCurrency
        self.selectedCurrencies = selectedCurrencies
    }
}

struct GetHistoryRatesResponse: Codable, Hashable {
    let base: String
    let end_date: String
    let rates: [String: [String: Double]]
    let start_date: String
    let success: Bool
    let timeseries: Bool
    
    init(base: String, end_date: String, rates: [String: [String: Double]], start_date: String, success: Bool, timeseries: Bool) {
        self.base = base
        self.end_date = end_date
        self.rates = rates
        self.start_date = start_date
        self.success = success
        self.timeseries = timeseries
    }
}
