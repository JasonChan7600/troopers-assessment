//
//  CurrencyWebService.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 30/05/2022.
//

import UIKit

class CurrencyWebService {
    
    func getAllCurrencyRates(_ requestBody: GetAllCurrencyRatesRequest, completion: @escaping(Result<GetAllCurrencyRatesResponse, APIError>) -> Void) {
        guard let url = URL(string: Essentials.baseUrl + "/latest?base=\(requestBody.baseCurrencyCode)")
            else {
                fatalError("URL is not correct!")
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue(Essentials.apikey, forHTTPHeaderField: "apikey")
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode == 200,
                let jsonData = data
            else {
                if let _ = error {
                    completion(.failure(.serverProblem))
                    return
                } else {
                    completion(.failure(.responseProblem))
                    return
                }
            }
            do {
                let responseData = try JSONDecoder().decode(GetAllCurrencyRatesResponse.self, from: jsonData)
                completion(.success(responseData))
            } catch {
                completion(.failure(.decodingProblem))
            }
        }
        dataTask.resume()
    }
    
    func getConvertedAmount(_ requestBody: GetConvertedAmountRequest, completion: @escaping(Result<GetConvertedAmountResponse, APIError>) -> Void) {
        guard let url = URL(string: Essentials.baseUrl + "/convert?to=\(requestBody.toCurrency)&from=\(requestBody.fromCurrency)&amount=\(requestBody.amount)")
            else {
                fatalError("URL is not correct!")
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue(Essentials.apikey, forHTTPHeaderField: "apikey")
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode == 200,
                let jsonData = data
            else {
                if let _ = error {
                    completion(.failure(.serverProblem))
                    return
                } else {
                    completion(.failure(.responseProblem))
                    return
                }
            }
            do {
                let responseData = try JSONDecoder().decode(GetConvertedAmountResponse.self, from: jsonData)
                completion(.success(responseData))
            } catch {
                completion(.failure(.decodingProblem))
            }
        }
        dataTask.resume()
    }
    
    func getHistoryRates(_ requestBody: GetHistoryRatesRequest, completion: @escaping(Result<GetHistoryRatesResponse, APIError>) -> Void) {
        guard let url = URL(string: Essentials.baseUrl + "/timeseries?start_date=\(requestBody.startDate)&end_date=\(requestBody.endDate)&base=\(requestBody.baseCurrency)&symbols=\(requestBody.selectedCurrencies)")
            else {
                fatalError("URL is not correct!")
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue(Essentials.apikey, forHTTPHeaderField: "apikey")
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode == 200,
                let jsonData = data
            else {
                if let _ = error {
                    completion(.failure(.serverProblem))
                    return
                } else {
                    completion(.failure(.responseProblem))
                    return
                }
            }
            do {
                let responseData = try JSONDecoder().decode(GetHistoryRatesResponse.self, from: jsonData)
                completion(.success(responseData))
            } catch {
                completion(.failure(.decodingProblem))
            }
        }
        dataTask.resume()
    }
    
    func getCurrencySymbols(completion: @escaping(Result<GetCurrencySymbolsResponse, APIError>) -> Void) {
        guard let url = URL(string: Essentials.baseUrl + "/symbols")
            else {
                fatalError("URL is not correct!")
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue(Essentials.apikey, forHTTPHeaderField: "apikey")
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode == 200,
                let jsonData = data
            else {
                if let _ = error {
                    completion(.failure(.serverProblem))
                    return
                } else {
                    completion(.failure(.responseProblem))
                    return
                }
            }
            do {
                let responseData = try JSONDecoder().decode(GetCurrencySymbolsResponse.self, from: jsonData)
                completion(.success(responseData))
            } catch {
                completion(.failure(.decodingProblem))
            }
        }
        dataTask.resume()
    }
    
}
