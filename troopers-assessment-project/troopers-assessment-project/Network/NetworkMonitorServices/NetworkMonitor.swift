//
//  NetworkMonitor.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 01/06/2022.
//

import Network

final class NetworkMonitor {
    static let shared = NetworkMonitor() // singleton
    
    private let queue = DispatchQueue.global()
    private let monitor: NWPathMonitor
    public private(set) var isConnected: Bool = false // allow public to read, but only this class can set the value
    public private(set) var connectionType: ConnectionType = .unknown // unused
    
    private init() {
        monitor = NWPathMonitor()
    }
    
    public func startMonitoring() { // called in AppDelegate
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = { [weak self] path in // called everytime internet connection changes
            self?.isConnected = path.status == .satisfied
            self?.getConnectionType(path)
        }
    }
    
    public func stopMonitoring() { // unused
        monitor.cancel()
    }
    
    private func getConnectionType(_ path: NWPath) { // unused
        if path.usesInterfaceType(.wifi) {
            connectionType = .wifi
        } else if path.usesInterfaceType(.cellular) {
            connectionType = .cellular
        } else if path.usesInterfaceType(.wiredEthernet) {
            connectionType = .ethernet
        } else {
            connectionType = .unknown
        }
    }
}
