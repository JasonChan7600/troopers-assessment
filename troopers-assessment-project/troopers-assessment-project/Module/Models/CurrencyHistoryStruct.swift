//
//  CurrencyHistoryStruct.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 01/06/2022.
//

struct CurrencyHistoryStruct: Codable, Hashable {
    let date: String
    let currencyValue: Double
}
