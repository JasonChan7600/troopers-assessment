//
//  CurrencySymbolsStruct.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 01/06/2022.
//

struct CurrencySymbolsStruct {
    let currencySymbol: String
    let currencyCountry: String
}
