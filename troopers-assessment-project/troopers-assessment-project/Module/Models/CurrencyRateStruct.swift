//
//  CurrencyRateStruct.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 01/06/2022.
//

struct CurrencyRateStruct {
    let currencyCode: String
    let currencyValue: Double
}
