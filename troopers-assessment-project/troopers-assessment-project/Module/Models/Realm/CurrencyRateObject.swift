//
//  CurrencyRateObject.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 01/06/2022.
//

import RealmSwift

class CurrencyRateObject: Object {
    
    @Persisted var currencyCode: String?
    @Persisted var currencyValue: Double?
    
}
