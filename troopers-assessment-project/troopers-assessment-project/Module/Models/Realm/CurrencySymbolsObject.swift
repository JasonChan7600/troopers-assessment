//
//  CurrencySymbolsObject.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 01/06/2022.
//

import RealmSwift

class CurrencySymbolsObject: Object {
    
    @Persisted var currencySymbol: String?
    @Persisted var currencyCountry: String?
    
}
