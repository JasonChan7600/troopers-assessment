//
//  CurrencyHistoryObject.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 01/06/2022.
//

import RealmSwift

class CurrencyHistoryObject: Object {
    
    @Persisted var date: String?
    @Persisted var currencyValue: Double?
    
}
