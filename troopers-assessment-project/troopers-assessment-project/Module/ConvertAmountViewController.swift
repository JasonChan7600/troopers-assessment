//
//  ConvertAmountViewController.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 30/05/2022.
//

import UIKit
import RealmSwift

class ConvertAmountViewController: UIViewController {
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var fromCurrencyCodeTextField: UITextField!
    @IBOutlet weak var toCurrencyCodeTextField: UITextField!
    @IBOutlet weak var convertButton: UIButton!
    @IBOutlet weak var amountLabel: UILabel!
    
    var fromCurrencyCodePicker = UIPickerView()
    var toCurrencyCodePicker = UIPickerView()
    
    var selectedFromCurrencyCode = ""
    var selectedToCurrencyCode = ""
    
    let realm = try! Realm()
    var currencySymbolsObjectArray = [CurrencySymbolsObject]()
    
    var currencySymbolsResponse = GetCurrencySymbolsResponse(success: false, symbols: [:]) {
        didSet {
            DispatchQueue.main.async {
                self.currencySymbolsArray.removeAll()
                self.currencySymbolsObjectArray.removeAll()
                
                for (_, keyValue) in self.currencySymbolsResponse.symbols.enumerated() {
                    let currencySymbols = CurrencySymbolsStruct(currencySymbol: keyValue.key, currencyCountry: keyValue.value)
                    self.currencySymbolsArray.append(currencySymbols)
                    
                    let currencySymbolsObject = CurrencySymbolsObject()
                    currencySymbolsObject.currencySymbol = keyValue.key
                    currencySymbolsObject.currencyCountry = keyValue.value
                    self.currencySymbolsObjectArray.append(currencySymbolsObject)
                }
                let currencySymbolsObjectResults = self.realm.objects(CurrencySymbolsObject.self)
                try! self.realm.write {
                    self.realm.delete(currencySymbolsObjectResults)
                }
                try! self.realm.write {
                    self.realm.add(self.currencySymbolsObjectArray)
                }
            }
        }
    }
    var currencySymbolsArray = [CurrencySymbolsStruct]() {
        didSet {
            DispatchQueue.main.async {
                self.sortedCurrencySymbolsArray = self.currencySymbolsArray.sorted(by: { $0.currencySymbol < $1.currencySymbol })
            }
        }
    }
    var sortedCurrencySymbolsArray = [CurrencySymbolsStruct]() {
        didSet {
            DispatchQueue.main.async {
                self.fromCurrencyCodePicker.reloadAllComponents()
                self.toCurrencyCodePicker.reloadAllComponents()
            }
        }
    }
    var convertedAmountResponse = GetConvertedAmountResponse(date: "", info: InfoObject(rate: 0, timestamp: 0), query: QueryObject(amount: 0, from: "", to: ""), result: 0, success: false) {
        didSet {
            DispatchQueue.main.async {
                self.amountLabel.text = "\(self.convertedAmountResponse.result)"
            }
        }
    }
    var apiError: APIError = .responseProblem {
        didSet {
            DispatchQueue.main.async {
                print("API Error: \(self.apiError)")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureBaseCurrencyCodePicker()
        configureFromCurrencyCodeTextField()
        configureToCurrencyCodeTextField()
        configureAmountTextField()
        configureBothCurrencyCodesToDefault()
        configureConvertButton()
        
        fetchCurrencySymbols()
    }
    
    func configureBaseCurrencyCodePicker() {
        fromCurrencyCodePicker.delegate = self
        toCurrencyCodePicker.delegate = self
        
        fromCurrencyCodePicker.tag = 1
        toCurrencyCodePicker.tag = 2
        
        fromCurrencyCodeTextField.inputView = fromCurrencyCodePicker
        toCurrencyCodeTextField.inputView = toCurrencyCodePicker
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissBaseCurrencyCodePicker))
        toolBar.setItems([doneBtn], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        fromCurrencyCodeTextField.inputAccessoryView = toolBar
        toCurrencyCodeTextField.inputAccessoryView = toolBar
    }
    
    @objc func dismissBaseCurrencyCodePicker() {
        view.endEditing(true)
    }
    
    func configureFromCurrencyCodeTextField() {
        fromCurrencyCodeTextField.text = getLocalCountryCode()
    }
    
    func configureToCurrencyCodeTextField() {
        toCurrencyCodeTextField.text = getLocalCountryCode()
    }
    
    func configureAmountTextField() {
        amountTextField.text = "1"
    }
    
    func configureBothCurrencyCodesToDefault() {
        selectedFromCurrencyCode = getLocalCountryCode()
        selectedToCurrencyCode = getLocalCountryCode()
    }
    
    func configureConvertButton() {
        convertButton.addTarget(self, action: #selector(convertButtonTapped), for: .touchUpInside)
    }
    
    @objc func convertButtonTapped() {
        let amount = amountTextField.text ?? "1"
        fetchConvertedAmount(fromCurrency: selectedFromCurrencyCode, toCurrency: selectedToCurrencyCode, amount: amount)
    }
    
    func getLocalCountryCode() -> String {
        let locale = Locale.current
        return locale.currencyCode ?? ""
    }
    
    func fetchCurrencySymbols() {
        if NetworkMonitor.shared.isConnected {
            getCurrencySymbols()
        } else {
            self.currencySymbolsArray.removeAll()
            let currencySymbolsObjectResults = realm.objects(CurrencySymbolsObject.self)
            
            for index in 0...currencySymbolsObjectResults.count - 1 {
                let currencySymbol = currencySymbolsObjectResults[index].currencySymbol ?? ""
                let currencyCountry = currencySymbolsObjectResults[index].currencyCountry ?? ""
                
                let currencySymbols = CurrencySymbolsStruct(currencySymbol: currencySymbol, currencyCountry: currencyCountry)
                self.currencySymbolsArray.append(currencySymbols)
            }
        }
    }
    
    func fetchConvertedAmount(fromCurrency: String?, toCurrency: String?, amount: String) {
        var newFromCurrency = ""
        var newToCurrency = ""
        
        if let fromCurrency = fromCurrency {
            newFromCurrency = fromCurrency
        } else {
            newFromCurrency = getLocalCountryCode()
        }
        
        if let toCurrency = toCurrency {
            newToCurrency = toCurrency
        } else {
            newToCurrency = getLocalCountryCode()
        }
        
        let getConvertedAmountRequest = GetConvertedAmountRequest(fromCurrency: newFromCurrency, toCurrency: newToCurrency, amount: amount)
        getConvertedAmount(request: getConvertedAmountRequest)
    }
    
}

extension ConvertAmountViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortedCurrencySymbolsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(sortedCurrencySymbolsArray[row].currencySymbol) - \(sortedCurrencySymbolsArray[row].currencyCountry)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedCurrencyCode = sortedCurrencySymbolsArray[row].currencySymbol
        
        if pickerView.tag == 1 {
            fromCurrencyCodeTextField.text = selectedCurrencyCode
            selectedFromCurrencyCode = selectedCurrencyCode
        } else if pickerView.tag == 2 {
            toCurrencyCodeTextField.text = selectedCurrencyCode
            selectedToCurrencyCode = selectedCurrencyCode
        }
    }
    
}

extension ConvertAmountViewController {
    
    func getCurrencySymbols() {
        CurrencyWebService().getCurrencySymbols(completion: { result in
            switch result {
            case .success(let responseData):
                self.currencySymbolsResponse = responseData
            case .failure(let error):
                self.apiError = error
            }
        })
    }
    
    func getConvertedAmount(request: GetConvertedAmountRequest) {
        CurrencyWebService().getConvertedAmount(request, completion: { result in
            switch result {
            case .success(let responseData):
                self.convertedAmountResponse = responseData
            case .failure(let error):
                self.apiError = error
            }
        })
    }
    
}
