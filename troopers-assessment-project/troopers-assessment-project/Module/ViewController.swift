//
//  ViewController.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 30/05/2022.
//

import UIKit
import Charts
import RealmSwift

class ViewController: UIViewController {

    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var baseCurrencyTextField: UITextField!
    @IBOutlet weak var currencyRatesTableView: UITableView!
    @IBOutlet weak var lineChartView: LineChartView!
    
    var baseCurrencyCodePicker = UIPickerView()
    var selectedBaseCurrencyCode = ""
    
    let realm = try! Realm()
    var currencyRateObjectArray = [CurrencyRateObject]()
    var currencySymbolsObjectArray = [CurrencySymbolsObject]()
    var currencyHistoryObjectArray = [CurrencyHistoryObject]()
    
    var currencyRatesResponse = GetAllCurrencyRatesResponse(base: "", date: "", rates: [:], success: false, timestamp: 0) {
        didSet {
            DispatchQueue.main.async {
                self.currencyRateArray.removeAll()
                self.currencyRateObjectArray.removeAll()
                
                for (_, keyValue) in self.currencyRatesResponse.rates.enumerated() {
                    
                    let currencyRate = CurrencyRateStruct(currencyCode: keyValue.key, currencyValue: keyValue.value)
                    self.currencyRateArray.append(currencyRate)
                    
                    let currencyRateObject = CurrencyRateObject()
                    currencyRateObject.currencyCode = keyValue.key
                    currencyRateObject.currencyValue = keyValue.value
                    self.currencyRateObjectArray.append(currencyRateObject)
                }
                let currencyRateObjectResults = self.realm.objects(CurrencyRateObject.self)
                try! self.realm.write {
                    self.realm.delete(currencyRateObjectResults)
                }
                try! self.realm.write {
                    self.realm.add(self.currencyRateObjectArray)
                }
            }
        }
    }
    var currencyRateArray = [CurrencyRateStruct]() {
        didSet {
            DispatchQueue.main.async {
                self.sortedCurrencyRateArray = self.currencyRateArray.sorted(by: { $0.currencyCode < $1.currencyCode })
            }
        }
    }
    var sortedCurrencyRateArray = [CurrencyRateStruct]() {
        didSet {
            DispatchQueue.main.async {
                self.currencyRatesTableView.reloadData()
            }
        }
    }
    var currencySymbolsResponse = GetCurrencySymbolsResponse(success: false, symbols: [:]) {
        didSet {
            DispatchQueue.main.async {
                self.currencySymbolsArray.removeAll()
                self.currencySymbolsObjectArray.removeAll()
                
                for (_, keyValue) in self.currencySymbolsResponse.symbols.enumerated() {
                    let currencySymbols = CurrencySymbolsStruct(currencySymbol: keyValue.key, currencyCountry: keyValue.value)
                    self.currencySymbolsArray.append(currencySymbols)
                    
                    let currencySymbolsObject = CurrencySymbolsObject()
                    currencySymbolsObject.currencySymbol = keyValue.key
                    currencySymbolsObject.currencyCountry = keyValue.value
                    self.currencySymbolsObjectArray.append(currencySymbolsObject)
                }
                let currencySymbolsObjectResults = self.realm.objects(CurrencySymbolsObject.self)
                try! self.realm.write {
                    self.realm.delete(currencySymbolsObjectResults)
                }
                try! self.realm.write {
                    self.realm.add(self.currencySymbolsObjectArray)
                }
            }
        }
    }
    var currencySymbolsArray = [CurrencySymbolsStruct]() {
        didSet {
            DispatchQueue.main.async {
                self.sortedCurrencySymbolsArray = self.currencySymbolsArray.sorted(by: { $0.currencySymbol < $1.currencySymbol })
            }
        }
    }
    var sortedCurrencySymbolsArray = [CurrencySymbolsStruct]() {
        didSet {
            DispatchQueue.main.async {
                self.baseCurrencyCodePicker.reloadAllComponents()
            }
        }
    }
    var historyRatesResponse = GetHistoryRatesResponse(base: "", end_date: "", rates: [:], start_date: "", success: false, timeseries: false) {
        didSet {
            DispatchQueue.main.async {
                self.currencyHistoryArray.removeAll()
                self.currencyHistoryObjectArray.removeAll()
                
                var date = ""
                var currencyValue = 0.0
                
                for (_, keyValueOuter) in self.historyRatesResponse.rates.enumerated() {
                    date = keyValueOuter.key
                    
                    for (_, keyValueInner) in keyValueOuter.value.values.enumerated() {
                        currencyValue = keyValueInner
                    }
                    let currencyHistoryStruct = CurrencyHistoryStruct(date: date, currencyValue: currencyValue)
                    self.currencyHistoryArray.append(currencyHistoryStruct)
                    
                    let currencyHistoryObject = CurrencyHistoryObject()
                    currencyHistoryObject.date = date
                    currencyHistoryObject.currencyValue = currencyValue
                    self.currencyHistoryObjectArray.append(currencyHistoryObject)
                }
                let currencyHistoryObjectResults = self.realm.objects(CurrencyHistoryObject.self)
                try! self.realm.write {
                    self.realm.delete(currencyHistoryObjectResults)
                }
                try! self.realm.write {
                    self.realm.add(self.currencyHistoryObjectArray)
                }
            }
        }
    }
    var currencyHistoryArray = [CurrencyHistoryStruct]() {
        didSet {
            DispatchQueue.main.async {
                self.sortedCurrencyHistoryArray = self.currencyHistoryArray.sorted(by: { $0.date < $1.date })
            }
        }
    }
    var sortedCurrencyHistoryArray = [CurrencyHistoryStruct]() {
        didSet {
            DispatchQueue.main.async {
                guard oldValue != self.sortedCurrencyHistoryArray else {
                    return
                }
                self.chartDataEntryArray.removeAll()
                
                for index in 0...self.sortedCurrencyHistoryArray.count - 1 {
                    let currencyValue = self.sortedCurrencyHistoryArray[index].currencyValue
                    let chartDataEntry = ChartDataEntry(x: Double(index), y: currencyValue)
                    self.chartDataEntryArray.append(chartDataEntry)
                }
            }
        }
    }
    var chartDataEntryArray = [ChartDataEntry]() {
        didSet {
            DispatchQueue.main.async {
                guard oldValue != self.chartDataEntryArray else {
                    return
                }
                self.setDataLineChartView()
            }
        }
    }
//    var chartDataEntryArray: [ChartDataEntry] = [
//        ChartDataEntry(x: 0.0, y: 10.0),
//        ChartDataEntry(x: 1.0, y: 5.0),
//        ChartDataEntry(x: 2.0, y: 7.0),
//        ChartDataEntry(x: 3.0, y: 5.0),
//        ChartDataEntry(x: 4.0, y: 10.0)
//    ]
    var apiError: APIError = .responseProblem {
        didSet {
            DispatchQueue.main.async {
                print("API Error: \(self.apiError)")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Realm.Configuration.defaultConfiguration.fileURL)
        
        configureNavigationBar()
        configureCurrencyRatesTableView()
        configureBaseCurrencyTextField()
        configureRefreshButton()
        configureBaseCurrencyCodePicker()
        configureLineChartView()
        
        fetchCurrencySymbols()
        fetchAllCurrencyRates(baseCurrencyCode: nil)
        fetchHistoryRates(baseCurrencyCode: nil, selectedCurrencyCode: nil)
    }
    
    func configureNavigationBar() {
        navigationItem.title = "Currency Converter"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Custom", style: .plain, target: self, action: #selector(navRightBarButtonTapped))
    }
    
    @objc func navRightBarButtonTapped() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let convertAmountViewController = mainStoryboard.instantiateViewController(identifier: "ConvertAmountViewController") as? ConvertAmountViewController
            else {
                print("Cant find the VC")
                return
        }
        self.navigationController?.pushViewController(convertAmountViewController, animated: true)
    }
    
    func configureCurrencyRatesTableView() {
        currencyRatesTableView.delegate = self
        currencyRatesTableView.dataSource = self
    }
    
    func configureBaseCurrencyTextField() {
        baseCurrencyTextField.text = getLocalCountryCode()
    }
    
    func configureRefreshButton() {
        refreshButton.addTarget(self, action: #selector(refreshButtonTapped), for: .touchUpInside)
    }
    
    @objc func refreshButtonTapped() {
        configureBaseCurrencyTextField()
        fetchCurrencySymbols()
        fetchAllCurrencyRates(baseCurrencyCode: nil)
        fetchHistoryRates(baseCurrencyCode: nil, selectedCurrencyCode: nil)
    }
    
    func configureBaseCurrencyCodePicker() {
        baseCurrencyCodePicker.delegate = self
        baseCurrencyTextField.inputView = baseCurrencyCodePicker
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissBaseCurrencyCodePicker))
        toolBar.setItems([doneBtn], animated: false)
        toolBar.isUserInteractionEnabled = true
        baseCurrencyTextField.inputAccessoryView = toolBar
    }
    
    @objc func dismissBaseCurrencyCodePicker() {
        fetchAllCurrencyRates(baseCurrencyCode: selectedBaseCurrencyCode)
        fetchHistoryRates(baseCurrencyCode: selectedBaseCurrencyCode, selectedCurrencyCode: selectedBaseCurrencyCode)
        view.endEditing(true)
    }
    
    func configureLineChartView() {
        lineChartView.backgroundColor = .systemBlue
        lineChartView.xAxis.labelPosition = .bottom
        //lineChartView.xAxis.enabled = false
    }
    
    func setDataLineChartView() {
        let set = LineChartDataSet(entries: chartDataEntryArray, label: "LOLOLOL")
        let data = LineChartData(dataSet: set)
        lineChartView.data = data
    }
    
    func getLocalCountryCode() -> String {
        let locale = Locale.current
        return locale.currencyCode ?? ""
    }
    
    func fetchCurrencySymbols() {
        if NetworkMonitor.shared.isConnected {
            getCurrencySymbols()
        } else {
            self.currencySymbolsArray.removeAll()
            let currencySymbolsObjectResults = realm.objects(CurrencySymbolsObject.self)
            
            for index in 0...currencySymbolsObjectResults.count - 1 {
                let currencySymbol = currencySymbolsObjectResults[index].currencySymbol ?? ""
                let currencyCountry = currencySymbolsObjectResults[index].currencyCountry ?? ""
                
                let currencySymbols = CurrencySymbolsStruct(currencySymbol: currencySymbol, currencyCountry: currencyCountry)
                self.currencySymbolsArray.append(currencySymbols)
            }
        }
    }
    
    func fetchAllCurrencyRates(baseCurrencyCode: String?) {
        var newCountryCode = ""
        
        if let countryCode = baseCurrencyCode {
            newCountryCode = countryCode
        } else {
            newCountryCode = getLocalCountryCode()
        }
        
        if NetworkMonitor.shared.isConnected {
            
            let getAllCurrencyRatesRequest = GetAllCurrencyRatesRequest(baseCurrencyCode: newCountryCode)
            getAllCurrencyRates(request: getAllCurrencyRatesRequest)
            
        } else {
            self.currencyRateArray.removeAll()
            let currencyRateObjectResults = realm.objects(CurrencyRateObject.self)
            
            for index in 0...currencyRateObjectResults.count - 1 {
                let currencyCode = currencyRateObjectResults[index].currencyCode ?? ""
                let currencyValue = currencyRateObjectResults[index].currencyValue ?? 1
                
                let currencyRate = CurrencyRateStruct(currencyCode: currencyCode, currencyValue: currencyValue)
                self.currencyRateArray.append(currencyRate)
            }
        }
        
    }
    
    func fetchHistoryRates(baseCurrencyCode: String?, selectedCurrencyCode: String?) {
        var newBaseCountryCode = ""
        var newSelectedCountryCode = ""
        
        if let countryCode = baseCurrencyCode {
            newBaseCountryCode = countryCode
        } else {
            newBaseCountryCode = getLocalCountryCode()
        }
        
        if let countryCode = selectedCurrencyCode {
            newSelectedCountryCode = countryCode
        } else {
            newSelectedCountryCode = getLocalCountryCode()
        }
        
        let todayUtcDateTime = HandleDateTime.getCurrentUTCDateTime()
        let todayFormattedDateTime = HandleDateTime.convertUTCDateToLocalDate(dateToConvert: todayUtcDateTime)
        
        let pastSevenDaysDateTime = Calendar.current.date(byAdding: .day, value: -6, to: Date())!
        let formattedPastSevenDaysDateTime = HandleDateTime.reformatLocalDate(dateToFormat: pastSevenDaysDateTime)
        
        if NetworkMonitor.shared.isConnected {
            
            let getHistoryRatesRequest = GetHistoryRatesRequest(endDate: todayFormattedDateTime, startDate: formattedPastSevenDaysDateTime, baseCurrency: newBaseCountryCode, selectedCurrencies: newSelectedCountryCode) // selectedCurrencies: GBP,JPY,EUR
            getHistoryRates(request: getHistoryRatesRequest)
            
        } else {
            self.currencyHistoryArray.removeAll()
            let currencyHistoryObjectResults = realm.objects(CurrencyHistoryObject.self)
            
            for index in 0...currencyHistoryObjectResults.count - 1 {
                let date = currencyHistoryObjectResults[index].date ?? ""
                let currencyValue = currencyHistoryObjectResults[index].currencyValue ?? 0
                
                let currencyHistory = CurrencyHistoryStruct(date: date, currencyValue: currencyValue)
                self.currencyHistoryArray.append(currencyHistory)
            }
        }
    }
    
}

extension ViewController: ChartViewDelegate {
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
    }
    
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortedCurrencySymbolsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(sortedCurrencySymbolsArray[row].currencySymbol) - \(sortedCurrencySymbolsArray[row].currencyCountry)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedBaseCurrencyCode = sortedCurrencySymbolsArray[row].currencySymbol
        baseCurrencyTextField.text = selectedBaseCurrencyCode
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCurrencyCode = sortedCurrencyRateArray[indexPath.row].currencyCode
        fetchHistoryRates(baseCurrencyCode: selectedBaseCurrencyCode, selectedCurrencyCode: selectedCurrencyCode)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedCurrencyRateArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        
        let currencyCode = sortedCurrencyRateArray[indexPath.row].currencyCode
        let currencyValue = sortedCurrencyRateArray[indexPath.row].currencyValue
        
        cell.textLabel?.text = "\(currencyCode) - \(currencyValue)"
        return cell
    }
    
}

extension ViewController {
    
    func getAllCurrencyRates(request: GetAllCurrencyRatesRequest) {
        CurrencyWebService().getAllCurrencyRates(request, completion: { result in
            switch result {
            case .success(let responseData):
                self.currencyRatesResponse = responseData
            case .failure(let error):
                self.apiError = error
            }
        })
    }
    
    func getHistoryRates(request: GetHistoryRatesRequest) {
        CurrencyWebService().getHistoryRates(request, completion: { result in
            switch result {
            case .success(let responseData):
                self.historyRatesResponse = responseData
            case .failure(let error):
                self.apiError = error
            }
        })
    }
    
    func getCurrencySymbols() {
        CurrencyWebService().getCurrencySymbols(completion: { result in
            switch result {
            case .success(let responseData):
                self.currencySymbolsResponse = responseData
            case .failure(let error):
                self.apiError = error
            }
        })
    }
    
}
