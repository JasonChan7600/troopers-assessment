//
//  Enums.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 30/05/2022.
//

enum APIError: Error {
    case responseProblem
    case decodingProblem
    case encodingProblem
    case serverProblem
}

enum ConnectionType {
    case wifi
    case cellular
    case ethernet
    case unknown
}
