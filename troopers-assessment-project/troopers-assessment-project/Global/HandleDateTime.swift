//
//  HandleDateTime.swift
//  troopers-assessment-project
//
//  Created by Jason Chan on 01/06/2022.
//

import Foundation

struct HandleDateTime {
    
    static func getCurrentUTCDateTime() -> String {
        let date = Date()
        let isoFormatter = ISO8601DateFormatter()
        return isoFormatter.string(from: date)
    }
    
    static func convertUTCDateToLocalDate(dateToConvert: String) -> String {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let convertedDate = format.date(from: dateToConvert)
        format.timeZone = TimeZone.current
        format.dateFormat = "yyyy-MM-dd"
        return format.string(from: convertedDate!)
    }
    
    static func reformatLocalDate(dateToFormat: Date) -> String {
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        return format.string(from: dateToFormat)
    }
    
}
