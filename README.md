# troopers-assessment



## Getting started

Please run "pod install" on the project folder named "troopers-assessment-project" before starting.

## Take Note

The number of requests remaining for this specific API Key is 47, if you need to use the app to make more requests, just create a new account in apilayer.com website you provided, subscribe to the free subscription, then put in the new API key in the Essentials.swift file.

Also, if you launch the app from an offline mode, and those data were loaded from the Realm database, and if you were to make it online and tap refresh, this will work precisely on a real device, meaning it'll load new data from the API, whereas it won't on a simulator.
